import React from 'react'
import logo from './logo.svg'
import './App.css'
import { Provider } from 'react-redux'

import store from './redux/store'
import CustomFormClass from './components/CustomFormClass'
import CustomFormFunc from './components/CustomFormFunc'
import Posts from './components/Posts'
import PostForm from './components/PostForm'

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Edit <code>src/App.js</code> and save to reload.
            </p>
          </header>
          <div style={{ display: 'flex', justifyContent: 'space-between', margin: '16px 0' }}>
            <CustomFormClass />
            <CustomFormFunc />
          </div>
          <PostForm />
          <hr style={{ width: '800px' }} />
          <Posts />
        </div>
      </Provider>
    )
  }
}

export default App
