import React from 'react'
import { Field, reduxForm } from 'redux-form'

class CustomFormClass extends React.Component {
  submit = values => {
    console.log('submit inside form')
    console.log('values', values)
  }

  render() {
    return (
      <>
        {/* handleSubmit is from redux */}
        <form onSubmit={this.props.handleSubmit(this.submit)}>
          <div>
            <label htmlFor="firsName">First Name</label>
            <Field name="firstName" component="input" type="text" />
          </div>
          <div>
            <label htmlFor="lastName">Last Name</label>
            <Field name="lastName" component="input" type="text" />
          </div>
          <div>
            <label htmlFor="email">Email</label>
            <Field name="email" component="input" type="email" />
          </div>
          <button type="submit">Submit</button>
        </form>
        <br />
      </>
    )
  }
}

CustomFormClass = reduxForm({
  form: 'customFormClass',
})(CustomFormClass)

export default CustomFormClass
