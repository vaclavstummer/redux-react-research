import React from 'react'
import { Field, reduxForm } from 'redux-form'

import CustomFormField from '../CustomFormField'

const submit = values => {
  console.log('submit inside form')
  console.log('values', values)
}

const CustomFormFunc = ({ handleSubmit }) => {
  return (
    <>
      {/* handleSubmit is from redux */}
      <form onSubmit={handleSubmit(submit)}>
        <Field name="firstName" label="First Name" component={CustomFormField} type="text" />
        <Field name="lastName" label="Last Name" component={CustomFormField} type="text" />
        <Field name="email" label="Email" component={CustomFormField} type="email" />
        <button type="submit">Submit</button>
      </form>
      <br />
    </>
  )
}

export default reduxForm({
  form: 'customFormFunc',
})(CustomFormFunc)
