import React from 'react'

const CustomFormField = ({ label, type, input, meta: { touched, error } }) => (
  <div className="input-row">
    <label>{label}</label>
    <input {...input} type={type} />
    {touched && error && <span className="error">{error}</span>}
  </div>
)

export default CustomFormField
